package com.tcwgq.vue2.learnvue2demo.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author tcwgq
 * @since 2023/9/5 21:09
 */
@Data
public class ArticleSearchVO {
    private Long id;

    private String title;

    private String content;

    private Date publishTime;

    private Integer commentCount;

    private Long authorId;

    private Integer pageNo = 1;

    private Integer pageSize = 10;

}
