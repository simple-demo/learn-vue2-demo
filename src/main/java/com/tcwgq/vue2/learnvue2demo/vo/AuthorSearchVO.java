package com.tcwgq.vue2.learnvue2demo.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author tcwgq
 * @since 2023/9/5 20:41
 */
@Data
public class AuthorSearchVO {
    private Long id;

    private String name;

    private Integer age;

    private String sex;

    private Date birthday;

    private Date createdTime;

    private Date updatedTime;

    private Integer pageNo;

    private Integer pageSize;

}
