package com.tcwgq.vue2.learnvue2demo.vo;

import lombok.Data;

import java.util.List;

/**
 * @author tcwgq
 * @since 2023/9/5 20:41
 */
@Data
public class CoverVO {
    private Integer type;

    private List<String> images;

}
