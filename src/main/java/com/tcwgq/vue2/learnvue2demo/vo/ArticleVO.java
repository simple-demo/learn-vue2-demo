package com.tcwgq.vue2.learnvue2demo.vo;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author tcwgq
 * @since 2023/9/5 21:09
 */
@Data
public class ArticleVO {
    private Long id;

    private String title;

    private String content;

    private Date publishTime;

    private Integer commentCount;

    private Long authorId;

    private String authorName;

    private CoverVO cover;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

}
