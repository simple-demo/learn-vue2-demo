package com.tcwgq.vue2.learnvue2demo.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author tcwgq
 * @since 2023/9/5 20:41
 */
@Data
@TableName("tb_cover")
public class Cover {
    @TableId(type = IdType.AUTO)
    private Long id;

    private Long articleId;

    private String image;

    private String alt;

    private Date createdTime;

    private Date updatedTime;

}
