package com.tcwgq.vue2.learnvue2demo.controller;

import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author tcwgq
 * @since 2023/9/8 17:08
 */
@ResponseBody
@ControllerAdvice
public class GlobalControllerAdvice {

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public ResponseResult<Void> handle(HttpMessageNotReadableException e) {
        return ResponseResult.fail(1111, e.getMessage());
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseResult<Void> handle(Exception e) {
        return ResponseResult.fail(1111, e.getMessage());
    }

}
