package com.tcwgq.vue2.learnvue2demo.controller;

import lombok.Data;

/**
 * @author tcwgq
 * @since 2023/9/5 21:04
 */
@Data
public class ResponseResult<T> {
    private Integer code;

    private String message;

    private T data;

    public static <T> ResponseResult<T> success(T data) {
        ResponseResult<T> responseResult = new ResponseResult<>();
        responseResult.setCode(0);
        responseResult.setMessage("success");
        responseResult.setData(data);
        return responseResult;
    }

    public static <T> ResponseResult<T> fail(Integer code, String message) {
        ResponseResult<T> responseResult = new ResponseResult<>();
        responseResult.setCode(code);
        responseResult.setMessage(message);
        return responseResult;
    }

}
