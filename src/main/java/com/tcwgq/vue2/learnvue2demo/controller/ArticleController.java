package com.tcwgq.vue2.learnvue2demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tcwgq.vue2.learnvue2demo.mapper.ArticleMapper;
import com.tcwgq.vue2.learnvue2demo.mapper.CoverMapper;
import com.tcwgq.vue2.learnvue2demo.model.Cover;
import com.tcwgq.vue2.learnvue2demo.vo.ArticleSearchVO;
import com.tcwgq.vue2.learnvue2demo.vo.ArticleVO;
import com.tcwgq.vue2.learnvue2demo.vo.CoverVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author tcwgq
 * @since 2023/9/5 21:03
 */
@Slf4j
@RestController
@RequestMapping("/api/article")
public class ArticleController {
    @Resource
    private ArticleMapper articleMapper;

    @Resource
    private CoverMapper coverMapper;

    @CrossOrigin
    @PostMapping("/page")
    public ResponseResult<PageInfo<ArticleVO>> page(@RequestBody ArticleSearchVO searchVO) {

        Page<ArticleVO> page = PageHelper.startPage(searchVO.getPageNo(), searchVO.getPageSize());
        articleMapper.findList(searchVO);

        PageInfo<ArticleVO> pageInfo = page.toPageInfo();
        List<ArticleVO> list = pageInfo.getList();

        Map<Long, List<Cover>> map = new HashMap<>();
        List<Long> articleIdList = list.stream().map(ArticleVO::getId).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(articleIdList)) {
            QueryWrapper<Cover> queryWrapper = new QueryWrapper<>();
            queryWrapper.in("article_id", articleIdList);
            List<Cover> covers = coverMapper.selectList(queryWrapper);
            map = covers.stream().collect(Collectors.groupingBy(Cover::getArticleId));
        }

        Map<Long, List<Cover>> finalMap = map;
        List<ArticleVO> dataList = list.stream().peek(articleVO -> {
            List<Cover> coverList = finalMap.getOrDefault(articleVO.getId(), Collections.emptyList());
            CoverVO coverVO = new CoverVO();
            coverVO.setType(coverList.size());
            coverVO.setImages(coverList.stream().map(Cover::getImage).collect(Collectors.toList()));
            articleVO.setCover(coverVO);
        }).collect(Collectors.toList());

        pageInfo.setList(dataList);

        return ResponseResult.success(pageInfo);
    }
}
