package com.tcwgq.vue2.learnvue2demo.controller;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tcwgq.vue2.learnvue2demo.mapper.AuthorMapper;
import com.tcwgq.vue2.learnvue2demo.model.Author;
import com.tcwgq.vue2.learnvue2demo.vo.AuthorSearchVO;
import com.tcwgq.vue2.learnvue2demo.vo.AuthorVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author tcwgq
 * @since 2023/9/7 15:54
 */
@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/api/author")
public class AuthorController {
    @Resource
    private AuthorMapper authorMapper;

    @PostMapping("/add")
    public ResponseResult<Boolean> add(@RequestBody Author author) {
        authorMapper.insert(author);

        return ResponseResult.success(true);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseResult<Boolean> delete(@PathVariable("id") Long id) {
        authorMapper.deleteById(id);

        return ResponseResult.success(true);
    }

    @PostMapping("/edit")
    public ResponseResult<Boolean> edit(@RequestBody Author author) {
        UpdateWrapper<Author> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", author.getId());
        authorMapper.update(author, updateWrapper);

        return ResponseResult.success(true);
    }

    @GetMapping("/get/{id}")
    public ResponseResult<Author> get(@PathVariable("id") Long id) {
        return ResponseResult.success(authorMapper.selectById(id));
    }

    @PostMapping("/page")
    public ResponseResult<PageInfo<AuthorVO>> page(@RequestBody AuthorSearchVO searchVO) {
        Page<AuthorVO> page = PageHelper.startPage(searchVO.getPageNo(), searchVO.getPageSize());
        authorMapper.findList(searchVO);

        return ResponseResult.success(page.toPageInfo());
    }

}
