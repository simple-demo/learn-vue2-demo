package com.tcwgq.vue2.learnvue2demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tcwgq.vue2.learnvue2demo.model.Author;
import com.tcwgq.vue2.learnvue2demo.vo.AuthorSearchVO;
import com.tcwgq.vue2.learnvue2demo.vo.AuthorVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author tcwgq
 * @since 2023/9/5 20:46
 */
@Mapper
public interface AuthorMapper extends BaseMapper<Author> {

    List<AuthorVO> findList(AuthorSearchVO searchVO);

}
