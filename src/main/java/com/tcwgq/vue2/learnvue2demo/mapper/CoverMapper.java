package com.tcwgq.vue2.learnvue2demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tcwgq.vue2.learnvue2demo.model.Cover;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tcwgq
 * @since 2023/9/5 20:47
 */
@Mapper
public interface CoverMapper extends BaseMapper<Cover> {

}
