package com.tcwgq.vue2.learnvue2demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tcwgq.vue2.learnvue2demo.model.Article;
import com.tcwgq.vue2.learnvue2demo.vo.ArticleSearchVO;
import com.tcwgq.vue2.learnvue2demo.vo.ArticleVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author tcwgq
 * @since 2023/9/5 20:46
 */
@Mapper
public interface ArticleMapper extends BaseMapper<Article> {
    List<ArticleVO> findList(ArticleSearchVO searchVO);

}
