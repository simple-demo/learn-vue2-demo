package com.tcwgq.vue2.learnvue2demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnVue2DemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(LearnVue2DemoApplication.class, args);
    }

}
