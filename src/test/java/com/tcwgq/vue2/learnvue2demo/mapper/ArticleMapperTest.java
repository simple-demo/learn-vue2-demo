package com.tcwgq.vue2.learnvue2demo.mapper;

import com.tcwgq.vue2.learnvue2demo.model.Article;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @author tcwgq
 * @since 2023/9/5 20:48
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ArticleMapperTest {
    @Resource
    private ArticleMapper articleMapper;

    @Test
    public void findAll() {
        List<Article> articles = articleMapper.selectList(null);
        for(Article article :articles){
            article.setAuthorId((long) new Random().nextInt(5)+1);
            article.setCommentCount(new Random().nextInt(1000));
            article.setPublishTime(new Date());
            articleMapper.updateById(article);
        }
        System.out.println(articles);
    }

}