package com.tcwgq.vue2.learnvue2demo.mapper;

import com.tcwgq.vue2.learnvue2demo.model.Author;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tcwgq
 * @since 2023/9/5 21:00
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class AuthorMapperTest {
    @Resource
    private AuthorMapper authorMapper;

    @Test
    public void findAll() {
        List<Author> authors = authorMapper.selectList(null);
        System.out.println(authors);
    }

}