package com.tcwgq.vue2.learnvue2demo.mapper;

import com.tcwgq.vue2.learnvue2demo.model.Cover;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;
import java.util.Random;

/**
 * @author tcwgq
 * @since 2023/9/5 21:00
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class CoverMapperTest {
    @Resource
    private CoverMapper coverMapper;

    @Test
    public void findAll() {
        List<Cover> covers = coverMapper.selectList(null);
        System.out.println(covers);
    }

    @Test
    public void batch() {
        for (int i = 0; i < 100; i++) {
            Cover cover = new Cover();
            // cover.setId();
            long l = (new Random().nextInt(61) + 1);
            cover.setArticleId(l);
            cover.setImage("https://www.baidu.com/img/PC_wenxin_1142bc061306e094e6eddaa3d9656145.gif");
            cover.setAlt("图片" + l);
            // cover.setCreatedTime();
            // cover.setUpdatedTime();
            coverMapper.insert(cover);
        }
    }

}